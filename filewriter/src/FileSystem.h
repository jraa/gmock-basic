#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <fstream>

class FileSystem
{
public:
    virtual ~FileSystem() {}
    virtual size_t write_to_storage(const std::string& filename, const std::string& content) {
        std::ofstream ofs(filename);
        ofs << content;
        ofs.close();
        return content.size ();
    }
};

#endif
