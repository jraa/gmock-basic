#ifndef FILEWRITER_H
#define FILEWRITER_H

#include <stdexcept>
#include <string>

#include "FileSystem.h"

class FileWriter
{
    FileSystem& _fs;

    std::string fix_filename(const std::string& filename) {
        const std::string extension = ".txt";
        const int size_ext = extension.size();
        const int size_fnm = filename.size();
        if (size_fnm >= size_ext and
            filename.compare(size_fnm, size_ext, extension) != 0) {
            return filename + extension;
        } else {
            return filename;
        }
    }

public:

    FileWriter(FileSystem& fs) :
        _fs(fs) {
    }

    void write(const std::string& filename, const std::string& content) {
        const std::string fn_fixed = fix_filename(filename);
        //const std::string fn_fixed = filename;
        _fs.write_to_storage(fn_fixed, content);
    }

    void write_check(const std::string& filename, const std::string& content) {
        const std::string fn_fixed = fix_filename(filename);
        //const std::string fn_fixed = filename;
        size_t v = _fs.write_to_storage(fn_fixed, content);
        if (v != content.size ()) {
            throw std::runtime_error("this is not right!");
        }
    }
};

#endif
