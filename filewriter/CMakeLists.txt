# nix-shell --pure
# cmake -S . -B build -DBUILD_TESTING=ON
# cmake --build build

cmake_minimum_required(VERSION 3.15)

project(filewriter LANGUAGES CXX)

add_subdirectory(src)

if(BUILD_TESTING)
  add_subdirectory(test)
endif()
