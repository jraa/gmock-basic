{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    cmake
    lcov
    python39
  ];

  shellHook = ''
    alias l='ls -al'
    alias ll='ls -l'
  '';

  LOCALE_ARCHIVE_2_27 = "${pkgs.glibcLocales}/lib/locale/locale-archive";
}
