#ifndef FAKEFILESYSTEM_H
#define FAKEFILESYSTEM_H

#include <string>

#include "gmock/gmock.h"

class FakeFileSystem : public FileSystem
{
public:
    MOCK_METHOD(
        size_t,
        write_to_storage,
        (const std::string& filename, const std::string& content),
        (override));
};

#endif
